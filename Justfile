set dotenv-load

build_game ARG:
	docker run -v "{{ARG}}":"/code" retrohive-builder

build:
	find "{{invocation_directory()}}"/Games/europe -maxdepth 1 -type d -print0 | xargs -0 -I{} just build_game {}

clean:
	find "{{invocation_directory()}}" -not -name '*.md' -not -name 'PKGBUILD' -not -wholename './.git*' -not -name 'Justfile' -not -name 'description.xml' -type f -print0 | xargs -0 -n1 rm -v

rsync FILE:
	rsync --rsync-path="sudo rsync" -avzr --inplace --progress "{{FILE}}" "$SSH_LOGIN@$SSH_HOST:$SSH_PATH"

deploy:
  find "{{invocation_directory()}}" -name '*.zst' -print0 | xargs --null -I {} just rsync "{}"
  ssh $SSH_LOGIN@$SSH_HOST "sudo ${SCRIPT}"
